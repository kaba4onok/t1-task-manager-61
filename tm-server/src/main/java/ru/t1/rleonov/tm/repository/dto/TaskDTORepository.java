package ru.t1.rleonov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import ru.t1.rleonov.tm.dto.model.TaskDTO;
import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    TaskDTO findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

}
